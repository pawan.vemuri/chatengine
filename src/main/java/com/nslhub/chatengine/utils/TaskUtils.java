package com.nslhub.chatengine.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.nslhub.chatengine.dao.TaskDAO;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatDEf;
import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.model.ChatTaskHistory;
import com.nslhub.chatengine.model.Def;
import com.nslhub.chatengine.model.Input;
import com.nslhub.chatengine.model.RoutingDef;
import com.nslhub.chatengine.model.Task;
import com.nslhub.chatengine.model.TaskHistory;
import com.nslhub.chatengine.participants.objectmapper.RoutingDefDeserializer;
import com.nslhub.chatengine.participants.types.ParallelParticipants;
import com.nslhub.chatengine.participants.types.SingleParticipant;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

public class TaskUtils {
	private static ch.qos.logback.classic.Logger LOGGER = TaskUtils.getLoger(TaskUtils.class);
	public static ChatTask createChatTaskFromDefAndInput(ChatDEf def, Input hwfInput) throws ChatflowException {
		ChatTask hwf = new ChatTask();
		hwf.taskid = TaskUtils.getGuid();
		hwf.uuid = TaskUtils.getGuid();
		hwf.wfDef = def;
		hwf.title = def.title;
		hwf.chatdefid = def.id;
		hwf.routingDef = getRoutingDef(def.flow);
		return hwf;
	}
	
	
		public static enum TASKACTIONS {
		COMPLETE,ERROR,INITIATE,UPDATE,RESTART
	}
	
	public static String getGuid()
	  {
	    return UUID.randomUUID().toString();
	  }

	public static void updatePayload(Task task, Def def) {
		/**
		 * TODO
		 * if the task payload is coming from an external call, such as a rest call on an url, make that call and update the task payload with the result.
		 */

	}

	public static RoutingDef getRoutingDef(String routingStr) throws ChatflowException {
		LOGGER.debug(routingStr);
		JSONObject obj = new JSONObject(routingStr);
		if(routingStr == null)
			return null;
		//routing can be specified as json or as xml in task.
		//parse, and return as routing def.
			RoutingDef def = new RoutingDef();
			JSONArray arr = obj.getJSONArray("participants");
			for(int count=0;count<arr.length();count++) {
				JSONObject next = arr.getJSONObject(count);
				if(next.getString("pType").equalsIgnoreCase("SINGLE")) {
					SingleParticipant participant = new SingleParticipant();
					if(next.has("choice"))
						participant.setChoice(next.getInt("choice"));
					if(next.has("url"))
						participant.setUrl(next.getString("url"));
					if(next.has("question"))
						participant.setQuestion(next.getString("question"));
					if(next.has("changeDriver"))
						participant.setVariable(next.getString("changeDriver"));
					if(next.has("desc"))
						participant.setDesc(next.getString("desc"));
					participant.setId(next.getInt("id"));
					def.addParticipant(participant);
				}
				else if(next.getString("pType").equalsIgnoreCase("PARALLEL")) {
					System.out.println(next.toString());
					ParallelParticipants participant = new ParallelParticipants();
					participant.setId(next.getInt("id"));
					if(next.has("choice"))
						participant.setChoice(next.getInt("choice"));
					if(next.has("url"))
						participant.setUrl(next.getString("url"));
					if(next.has("question"))
						participant.setQuestion(next.getString("question"));
					def.addParticipant(participant);
					if(next.has("participants")) {
						JSONArray participants = next.getJSONArray("participants");
						for(int idx=0;idx<participants.length();idx++) {
							JSONObject pChild = participants.getJSONObject(idx);
							if(pChild.getString("pType").equalsIgnoreCase("SINGLE")) {
								SingleParticipant sp = new SingleParticipant();
								sp.setId(next.getInt("id"));
								if(pChild.has("choice"))
									sp.setChoice(pChild.getInt("choice"));
								if(pChild.has("url"))
									sp.setUrl(pChild.getString("url"));
								if(pChild.has("question"))
									sp.setQuestion(pChild.getString("question"));
								if(pChild.has("changeDriver"))
									sp.setVariable(pChild.getString("changeDriver"));
								if(pChild.has("desc"))
									sp.setDesc(pChild.getString("desc"));
								participant.addParticipant(sp);
							}
						}
					}
				}
			}
			return def;
	}
	public static void main(String[] a) throws ChatflowException,Exception{
		String s = RoutingDefDeserializer.getContents();
		RoutingDef def = getRoutingDef(s);
		int x1 = 0;
	}


	 
	
	public static ChatTask getTask(String id) throws ChatflowException {
		return TaskDAO.getInstance().findById(id);
	}

	public static String convertISToString(InputStream inputStream) throws IOException {
	    if(inputStream == null)
	    	return null;
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;

		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
		}

		return stringBuilder.toString();
	}

	
	 public static Timestamp toTimestamp(Calendar calendar) {
		if(calendar == null)
			return null;
		else {
			return new java.sql.Timestamp(calendar.getTime().getTime());
		}
	}
	
	public static String getFormattedExceptionMessage(int index,Object[] args){
		return "";
	}


	public static void copyProperties(Task t, TaskHistory h) {
		h.action = t.action;
		h.groupTaskId = t.groupTaskId;
		h.blockIdx = t.blockIdx;
		h.createdDate = t.createdDate;
		h.currentAssignee = t.currentAssignee;
		h.currentAssigneeType = t.currentAssigneeType;
		h.endDate = t.endDate;
		h.expirationDate = t.expirationDate;
		h.hwfdefid = t.hwfdefid;
		h.orgId = t.orgId;
		h.outcome = t.outcome;
		h.participantIdx = t.participantIdx;
		h.participantType= t.participantType;
		h.payloadId = t.payloadId;
		h.routingId = t.routingId;
		h.sequentialIdx = t.sequentialIdx;
		h.state = t.state;
		h.taskId = t.taskId;
		h.taskNumber = t.taskNumber;
		h.title = t.title;
		h.updatedDate = t.updatedDate;
		h.uuid = t.uuid;
		h.version = t.version;
		h.versionComment = t.versionComment;
		h.votingStr = t.votingStr;


	}
	public static Calendar getCurrentDate() {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.MILLISECOND,0);
        return now;
    }
	
	public static void copyProperties(ChatTask t, ChatTaskHistory h) {
		h.orgId = t.orgid;
		h.participantIdx = t.participantIdx;
		h.sequentialIdx = t.sequentialIdx;
		h.state = t.state;
		h.taskId = t.taskid;
		h.chatdefid = t.chatdefid;
		h.title = t.title;
		h.uuid = t.uuid;
	}

	public static String getOrg(){
		return "milkyWay";
	}
	public static Logger getLoger(Class clazz){
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(clazz);
		logger.setLevel(Level.DEBUG);
		return logger;
	}
	
	
}
