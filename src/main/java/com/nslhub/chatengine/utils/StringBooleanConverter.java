package com.nslhub.chatengine.utils;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StringBooleanConverter implements AttributeConverter<Boolean, String> {

  @Override
  public String convertToDatabaseColumn(Boolean in) {
	  if(in)
		  return "Y";
    return "N"; 
  }

  @Override
  public Boolean convertToEntityAttribute(String in) {
	  if(in == null)
		  return false;
	  if(in.trim().equalsIgnoreCase("Y"))
		  return true;
    return false;
  }

}