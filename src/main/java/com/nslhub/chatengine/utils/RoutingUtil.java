package com.nslhub.chatengine.utils;

public class RoutingUtil {

	
	
	public static boolean isDynamic(String expression) {
		if(expression!=null && (expression.startsWith("eval(") || expression.startsWith("ldap(") || expression.startsWith("drl(")))
			return true;
		return false;
	}
	
	public static boolean isXPath(String expression) {
		if(expression!=null && expression.startsWith("eval("))
			return true;
		return false;
	}
	
	public static boolean isLdap(String expression) {
		if(expression!=null && expression.startsWith("ldap("))
			return true;
		return false;
	}
	
	public static boolean isRule(String expression) {
		if(expression!=null && expression.startsWith("drl("))
			return true;
		return false;
	}

	

	
}
