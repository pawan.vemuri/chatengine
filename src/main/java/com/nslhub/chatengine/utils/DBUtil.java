package com.nslhub.chatengine.utils;

import java.sql.Connection;
import java.sql.DriverManager;



public class DBUtil {

	private static String JDBC_URL="jdbc:mysql://localhost:3306/wf?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
	private static String JDBC_USER="root";
	private static String JDBC_PASSWORD="root";
	//TODO, check how properties are loaded and refactor this loading
	public static Connection getDBConnection() {
		try {
			//DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			Connection con = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
			return con;
		} catch (Exception e) {
			//e.printStackTrace();
			//throw new NonRecoverableHwfException("unable to create connection", e);
		}
		return null;
	}
	
}
