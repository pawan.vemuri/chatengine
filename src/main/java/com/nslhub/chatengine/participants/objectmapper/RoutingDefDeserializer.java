package com.nslhub.chatengine.participants.objectmapper;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.nslhub.chatengine.model.RoutingDef;
import com.nslhub.chatengine.participants.IParticipant;
import com.nslhub.chatengine.participants.types.AbstractParticipant;
import com.nslhub.chatengine.participants.types.ParallelParticipants;
import com.nslhub.chatengine.participants.types.SerialParticipant;
import com.nslhub.chatengine.participants.types.SingleParticipant;
public class RoutingDefDeserializer extends StdDeserializer<RoutingDef> { 
 
    public RoutingDefDeserializer() { 
        this(null); 
    } 
 
    public RoutingDefDeserializer(Class<?> vc) { 
        super(vc); 
    }
 
    @Override
	public RoutingDef deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
    	
		JsonNode node = jp.getCodec().readTree(jp);// mapper.readTree(x1);//jp.getCodec().readTree(jp);
		RoutingDef routingDef = new RoutingDef();
		if (node.get("participants") instanceof ArrayNode) {
			ArrayNode participants = (ArrayNode) node.get("participants");
			for (int y = 0; y < participants.size(); y++) {
				JsonNode participant = participants.get(y).get("participant");
				IParticipant participantObj = visitParticipant(participant);
				if (participantObj != null)
					routingDef.addParticipant(participantObj);
			}
		} 
		// RoutingDef x = TaskUtils.getRoutingDef(xml, null);
		// return x;

		return routingDef;
	}
    
    private IParticipant visitParticipant(JsonNode participant) {
    	String type = participant.get("pType").textValue();
		if(type.equals(IParticipant.PARTICIPANTTYPES.SINGLE.toString())){
			SingleParticipant sp = getSingleTypeParticipant(participant);
			return sp;
		} else if(type.equals(IParticipant.PARTICIPANTTYPES.SERIAL.toString())){
			SerialParticipant sp = getSequentialTypeParticipant(participant);
			return sp;
		}  else if(type.equals(IParticipant.PARTICIPANTTYPES.PARALLEL.toString())){
			ParallelParticipants sp = getParallelTypeParticipant(participant);
			return sp;
		} 
		return null;
	}

	private void fillValues(JsonNode node,AbstractParticipant participant){
    	String s = "pawan";
		
    }
   
	private ParallelParticipants getParallelTypeParticipant(JsonNode node) {
		ParallelParticipants parallelParticipants = new ParallelParticipants();
		JsonNode participantsNode = node.get("participants");
		if(participantsNode instanceof ArrayNode){
			addParallelChildren((ArrayNode)participantsNode,parallelParticipants);
		} else {
			IParticipant participantObj = visitParticipant(participantsNode.get("participant"));
    		if(participantObj!=null)
    			parallelParticipants.addParticipant(participantObj);
		}
		return parallelParticipants;
	}

	private void addParallelChildren(ArrayNode participantsNode, ParallelParticipants parallelParticipants) {
		for(int y=0;y<participantsNode.size();y++){
    		JsonNode participant = participantsNode.get(y).get("participant");
    		IParticipant participantObj = visitParticipant(participant);
    		if(participantObj!=null)
    			parallelParticipants.addParticipant(participantObj);
    	}
		
	}

	

	private SerialParticipant getSequentialTypeParticipant(JsonNode node) {
		SerialParticipant sequentialParticipant = new SerialParticipant();
		fillValues(node,sequentialParticipant);
		return sequentialParticipant;
	}

	private SingleParticipant getSingleTypeParticipant(JsonNode node) {
		SingleParticipant singleParticipant = new SingleParticipant();
		fillValues(node,singleParticipant);
		return singleParticipant;
	}
	
	public static String getContents() throws Exception {
		InputStream is = new FileInputStream("D:/hwf/thirdwingrest/src/test/resources/data/json/sequentialParticipant/sequentialParticipantRouting1.json");
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		String line = buf.readLine(); 
		StringBuilder sb = new StringBuilder();
		while(line != null){
			sb.append(line).append("\n");
			line = buf.readLine(); 
		} 
		String fileAsString = sb.toString();
		buf.close();
		return fileAsString;
	}
}