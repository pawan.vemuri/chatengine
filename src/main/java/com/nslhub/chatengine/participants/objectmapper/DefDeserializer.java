package com.nslhub.chatengine.participants.objectmapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.nslhub.chatengine.model.Def;
public class DefDeserializer extends StdDeserializer<Def> { 
 
    public DefDeserializer() { 
        this(null); 
    } 
 
    public DefDeserializer(Class<?> vc) { 
        super(vc); 
    }
 
    @Override
	public Def deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
    	InputStreamReader isReader = new InputStreamReader((InputStream)jp.getInputSource());
        //Creating a BufferedReader object
        BufferedReader reader = new BufferedReader(isReader);
        StringBuffer sb = new StringBuffer();
        String str;
        while((str = reader.readLine())!= null){
           sb.append(str);
        }
    	Def def = new Def();
		return super.deserialize(jp, ctxt, def);
	}
    
   
}