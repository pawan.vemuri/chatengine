package com.nslhub.chatengine.participants.objectmapper;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.nslhub.chatengine.model.RoutingDef;
import com.nslhub.chatengine.participants.IParticipant;
import com.nslhub.chatengine.participants.types.AbstractParticipant;
import com.nslhub.chatengine.participants.types.ParallelParticipants;
import com.nslhub.chatengine.participants.types.SerialParticipant;
import com.nslhub.chatengine.participants.types.SingleParticipant;
public class RoutingDefSerializer extends StdSerializer<RoutingDef> { 
 
    public RoutingDefSerializer() { 
        this(null); 
    } 
    public RoutingDefSerializer(Class<RoutingDef> t) {
        super(t);
    }
    @Override
    public void serialize(
    		RoutingDef value, JsonGenerator jgen, SerializerProvider provider) 
      throws IOException, JsonProcessingException {
  
        jgen.writeStartObject();
       jgen.writeStartObject();
 	    //jgen.writeEndArray();
        
        jgen.writeEndObject();
    }
 
      private void writeParticipant(IParticipant p, JsonGenerator jgen) throws IOException {
		jgen.writeStartObject();
		jgen.writeFieldName("participant");
		if(p instanceof SingleParticipant){
			writeSingleParticipant((SingleParticipant)p,jgen);
		} else if(p instanceof SerialParticipant){
			writeSequentialParticipant((SerialParticipant)p,jgen);
		} else if(p instanceof ParallelParticipants){
			writeParallelParticipants((ParallelParticipants)p,jgen);
		} 
		jgen.writeEndObject();
		
	}
	
	private void writeCommonAttributes(IParticipant p, JsonGenerator jgen) throws IOException {
		jgen.writeStringField("name", p.getName());
		
		
		
	}
	private void writeParallelParticipants(ParallelParticipants p, JsonGenerator jgen) throws IOException {
		jgen.writeStartObject();
		jgen.writeStringField("pType", IParticipant.PARTICIPANTTYPES.PARALLEL.toString());
		jgen.writeArrayFieldStart("participants");
  	    for(IParticipant p1 : p.participants){
		   writeParticipant(p1,jgen);
  	    }
  	    /**
	    for(IParticipant child :p.participants){
			if(child instanceof SingleParticipant){
				writeSingleParticipant((SingleParticipant)child,jgen);
			} else if(child instanceof SequentialParticipant){
				writeSequentialParticipant((SequentialParticipant)child,jgen);
			} else if(child instanceof ParallelParticipants){
				writeParallelParticipants((ParallelParticipants)child,jgen);
			} else if(child instanceof Notify){
				writeNotify((Notify)child,jgen);
			}
		}*/
	    jgen.writeEndArray();
 	    jgen.writeEndObject();
		
	}
	private void writeSequentialParticipant(SerialParticipant p, JsonGenerator jgen) throws IOException {
		jgen.writeStartObject();
		writeCommonAttributes(p,jgen);
		jgen.writeStringField("pType", IParticipant.PARTICIPANTTYPES.SERIAL.toString());
		jgen.writeEndObject();
		
	}
	private void writeSingleParticipant(SingleParticipant p, JsonGenerator jgen) throws IOException {
		jgen.writeStartObject();
		writeCommonAttributes(p,jgen);
		jgen.writeStringField("pType", IParticipant.PARTICIPANTTYPES.SINGLE.toString());
		jgen.writeEndObject();
		
	}
	private void fillValues(JsonNode node,AbstractParticipant participant){
    	String s = "pawan";
    }
   

	private ParallelParticipants getParallelTypeParticipant(JsonNode participant) {
		ParallelParticipants notify = new ParallelParticipants();
		//fillValues(node,notify);
		return notify;
	}

	private SerialParticipant getSequentialTypeParticipant(JsonNode node) {
		SerialParticipant sequentialParticipant = new SerialParticipant();
		fillValues(node,sequentialParticipant);
		return sequentialParticipant;
	}

	private SingleParticipant getSingleTypeParticipant(JsonNode node) {
		SingleParticipant singleParticipant = new SingleParticipant();
		fillValues(node,singleParticipant);
		return singleParticipant;
	}

	private static String getContents() throws Exception {
		InputStream is = new FileInputStream("d:/testjson.txt");
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		String line = buf.readLine(); 
		StringBuilder sb = new StringBuilder();
		while(line != null){
			sb.append(line).append("\n");
			line = buf.readLine(); 
		} 
		String fileAsString = sb.toString();
		buf.close();
		return fileAsString;
	}
}