package com.nslhub.chatengine.participants;

public interface IParticipant extends IRoutingEntity {
	public String getName();
	
	public String getType();
	
	/**
	 *
	 * @author pawan
	 * Types of participants that single, sequential, parallel, notify and adhoc can support
	 * a) single
	        1) a single user can be a one or more users seperated by commas.
			   if more than one user is specified, task will be assigned to all the users.
			   however, one of the assigned users should first claim the task and then act on it
			2) a single user can also be assigned to an ldap function. if the function returns more than one user,task will be assigned to all the users.
			   however, one of the assigned users should first claim the task and then act on it
			3)  a single user can also be assigned to an rule call. if the rule call returns more than one user,task will be assigned to all the users.
			   however, one of the assigned users should first claim the task and then act on it

       b) adhoc
       		1) not supported for now

       c) notify.
       		1) a notify user can be a one or more users seperated by commas.
			   if more than one user is specified, notify task will be assigned to all the users.
			2) a notify user can also be assigned to an ldap function. if the function returns more than one user,task will be assigned to all the users.
			3)  a notify user can also be assigned to an rule call. if the rule call returns more than one user,task will be assigned to all the users.

		d) sequential (debate this further)
		TODO
			1) a sequential list of assignees can be a comma seperated list of
			user/ldap lookup/ rule invocation ( which can in turn result in an ldap lookup). All these assignments will be broken down till we get a list of users and
			task will be assigned to these list of users one by one.
		e)

	 */
	public static enum PARTICIPANTTYPES {
		SINGLE,SERIAL,PARALLEL,NOTIFY,ADHOC
	}

}
