package com.nslhub.chatengine.participants.visitor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.json.JSONObject;

import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.dao.ChatDefDAO;
import com.nslhub.chatengine.dao.ChatVariablesDAO;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatDEf;
import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.model.ChatVariables;
import com.nslhub.chatengine.model.RoutingDef;
import com.nslhub.chatengine.model.Task;
import com.nslhub.chatengine.participants.IPartcicipantTypesVisitor;
import com.nslhub.chatengine.participants.IParticipant;
import com.nslhub.chatengine.participants.IRoutingEntity;
import com.nslhub.chatengine.participants.types.AbstractParticipant;
import com.nslhub.chatengine.participants.types.ParallelParticipants;
import com.nslhub.chatengine.participants.types.SerialParticipant;
import com.nslhub.chatengine.participants.types.SingleParticipant;
import com.nslhub.chatengine.utils.TaskUtils;


public class ParticipantsVisitor implements IPartcicipantTypesVisitor {
	
	ch.qos.logback.classic.Logger logger = TaskUtils.getLoger(ParticipantsVisitor.class);
	protected int blockIndex = 0;
	private RoutingDef routingDef = null;
	private ChatTask task;
	// mCurrentParticipantIndex --> blocks
  	public ParticipantsVisitor(ChatTask task,RoutingDef routingDef,CommandContext commandContext){
  		this.routingDef = routingDef;
  		this.task = task;
  		//logger.debug(debug());
  	}

  	
	public String _visit(SingleParticipant participant) throws ChatflowException {
  		logger.debug("action "+task.action);
  		String mTaskAction = task.action;
  		//todo
  		//invoke the url
  		String question = participant.question;
  		if(question.indexOf("[")!=-1) {
  			question = extracted(question);
  			return question;
  		}
		return participant.question;

	}


	private String extracted(String question) throws ChatflowException {
		String token = question.substring(question.indexOf("[")+1,question.indexOf("]"));
		if(token.indexOf("$(")!=-1) {
			String var = token.substring(token.indexOf("$(")+2,token.indexOf(")"));
			String s = "";
			ChatVariables variables = ChatVariablesDAO.getInstance().findById(task.taskid);
			JSONObject obj = new JSONObject(variables.variables);
			String val = obj.get(var).toString();
			token = token.replace("$("+var+")", val);//+"234567890";
			try {
				String urlRet = doInvoke(token);
				question = question.substring(0,question.indexOf("["));
				question = question+urlRet;
				return question;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return question;
	}

  	private String doInvoke(String url) throws Exception {
  		 URL urlForGetRequest = new URL(url);
  	    String readLine = null;
  	    HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
  	    conection.setRequestMethod("GET");
  	    int responseCode = conection.getResponseCode();
	  	  if (responseCode == HttpURLConnection.HTTP_OK) {
	          BufferedReader in = new BufferedReader(
	              new InputStreamReader(conection.getInputStream()));
	          StringBuffer response = new StringBuffer();
	          while ((readLine = in .readLine()) != null) {
	              response.append(readLine);
	          } in .close();
	          return response.toString();
	      } else {
	          System.out.println("GET NOT WORKED");
	      }
	  	  return null;
	}


	public boolean visit(SerialParticipant participant, List<Task> collectedTasks) throws ChatflowException {
  		logger.debug("action "+task.action);
  		task.createdDate = TaskUtils.getCurrentDate();
  		task.outcome = null;
  		return true;
	}

  

  	public String visit(ParallelParticipants participant) throws ChatflowException {
  		String mTaskAction = task.action;
  		task.createdDate = TaskUtils.getCurrentDate();
  		task.outcome = null;
  		//new HwfTaskDAO().insert(task);
  		StringBuffer sb = new StringBuffer();
  		for(IParticipant child : participant.participants) {
  			if(child instanceof AbstractParticipant) {
  				AbstractParticipant sp = (AbstractParticipant)child;
  				sb.append("choose "+sp.getChoice()+" for  "+sp.getDesc()+"\n");
  			}
  		}
  		return sb.toString();
	}

	
	


	
	protected List<IParticipant> getPartcipantsInCurrentBlock() {
		if(this.routingDef.getParticipants()==null ) {
			return null;
		}
	    List<IParticipant> list = this.routingDef.getParticipants();
	    return list;
	}

	/**
	 * the heart of the routing algorithm.
	 * 
	 * 3 cases arise here,
	 * 		1. when both the blockidx and participant idx ==0
	 *  	2. when only participant idx==0
	 *   	3. when both of these are >1.
	 *   Note that block idx as 0 and a numeric participant idx is not possible as we should be in a block to be able to execute a participant
	 * 1. when both the blockidx and participant idx == 0
	 * 		after the initiation, both block and participant would be 0.
	 * 		In this scenario, this method should return the first set of participant/s , i.e what is definied as the 1'st participant in 1'st block.
	 * 2. when only participant idx == 0
	 * 		this means that we are in the begining of the current block and we should move to the next participant in this block, if available.
	 * 		the method should return the next participant or none if we have moved past the last participant.
	 * 3. when both of these are >1
	 * 		If participant idx is of the same value of the block size, we have to move to the next block.
	 * 			set the particpant index back to 0 and return the next block or null if none avaibale.
	 * 		else, return the next participant in this block.
	 *
	 * * @return
	 * @throws ChatflowException
	 */
	public IRoutingEntity getNextParticipantOrBlock() throws ChatflowException{
	    IParticipant participantOrBlock = null;
	    
	    logger.debug("action "+task.action);
  		
	    if (task.participantIdx == 0) {
	        // no participants are being evaluated, that means that we are transitioning from one block to the other or are entering the first block
	    	//task.participantIdx = task.participantIdx+1;
	    	List<IParticipant> list = getPartcipantsInCurrentBlock();
	    	if(list == null)
	    		return null;
	        // we are in a block, get the next partivcipant in  that block
	    	IParticipant participant = list.get(task.participantIdx);
	    	return participant;
	    }else {
	    	IParticipant participant = getNextParticipant();
	    	return participant;
	    }
	    
	}

	/**
	 * todo
	 * make this a recusrsive call
	 * @return
	 * @throws ChatflowException
	 */
	private IParticipant getNextParticipant() throws ChatflowException {
		ChatDefDAO hwfDefDAO =  ChatDefDAO.getInstance();
		ChatDEf def = hwfDefDAO.findById(task.chatdefid);
		RoutingDef routingDef = TaskUtils.getRoutingDef(def.flow);
		int currentParticipantIdsx = task.participantIdx;
		for(int count=0;count<routingDef.participants.size();count++) {
			IParticipant partcicpant = routingDef.participants.get(count);
			if ((partcicpant instanceof ParallelParticipants) && partcicpant.getId() == currentParticipantIdsx) {
				ParallelParticipants pp = (ParallelParticipants)partcicpant;
				for(int pos=0;pos<pp.participants.size();pos++) {
					AbstractParticipant child = (AbstractParticipant)pp.participants.get(pos);
					int selectedChoice = Integer.parseInt(task.outcome);
					if(child.getChoice() == selectedChoice) {
						return child;
					} 
				}
			} else if(partcicpant.getId() == currentParticipantIdsx) {
				return routingDef.participants.get(count+1);
			} 
		}
		return null;
	}
	
	public IParticipant getCurrentParticipant() throws ChatflowException {
		ChatDefDAO hwfDefDAO =  ChatDefDAO.getInstance();
		ChatDEf def = hwfDefDAO.findById(task.chatdefid);
		RoutingDef routingDef = TaskUtils.getRoutingDef(def.flow);
		int currentParticipantIdsx = task.participantIdx;
		for(int count=0;count<routingDef.participants.size();count++) {
			IParticipant partcicpant = routingDef.participants.get(count);
			if(partcicpant.getId() == currentParticipantIdsx)
				return partcicpant;
		}
		return null;
	}


	protected void resetParticipantIndex(int index) {
		task.participantIdx = index;
		 
	}

	public String visit(IParticipant nextParticipant) throws ChatflowException {
		   if(nextParticipant instanceof SingleParticipant){
			   return  _visit((SingleParticipant)nextParticipant);
		   } else if(nextParticipant instanceof ParallelParticipants){
			   return visit((ParallelParticipants)nextParticipant);
		   }else if(nextParticipant instanceof SerialParticipant){
			   return visit((SerialParticipant)nextParticipant);
		   } else {
			   throw new ChatflowException("");
		   }
	}

}
