package com.nslhub.chatengine.participants.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nslhub.chatengine.participants.objectmapper.RoutingDefDeserializer;
import com.nslhub.chatengine.participants.objectmapper.RoutingDefSerializer;
@JsonDeserialize(using = RoutingDefDeserializer.class)
@JsonSerialize(using = RoutingDefSerializer.class)
public class RoutingDef {
	public String pType;
	@JsonIgnore
	public boolean modified = false;
	
}
