package com.nslhub.chatengine.participants.types;

import com.nslhub.chatengine.participants.IParticipant;

public class SingleParticipant extends AbstractParticipant {
	@Override
	public String getType() {
		return IParticipant.PARTICIPANTTYPES.SINGLE.toString();
	}
}
