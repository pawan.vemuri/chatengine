package com.nslhub.chatengine.participants.types;

import com.nslhub.chatengine.participants.IParticipant;

public abstract class AbstractParticipant implements IParticipant {
	private String name;
	 private String type;
	 private int Id;
	 private int choice;
	 private String variable;
	 private String desc;
	 public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getChoice() {
		return choice;
	}

	public void setChoice(int choice) {
		this.choice = choice;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String url;
	  public String question;
	
	@Override
	public String getName() {
		return name;
	}
	
	public String getParticipantType() {
		return type;
	}
	
	@Override
	public int getId() {
		return this.Id;
	}
	public void setId(int id) {
		this.Id = id;
	}
	
}
