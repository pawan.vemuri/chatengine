package com.nslhub.chatengine.participants.types;

import java.util.ArrayList;
import java.util.List;

import com.nslhub.chatengine.participants.IParticipant;

public class ParallelParticipants extends AbstractParticipant {
	public String name;
	
	public List<IParticipant> participants;
	public List<IParticipant> getParticipants(){
		return participants;
		
	}
	public void addParticipant(IParticipant participant){
		if(this.participants == null)
			this.participants = new ArrayList<IParticipant>();
		this.participants.add(participant);
		
	}
	@Override
	public String getName() {
		return null;
	}
	@Override
	public String getType() {
		return IParticipant.PARTICIPANTTYPES.PARALLEL.toString();
	}
	
}
