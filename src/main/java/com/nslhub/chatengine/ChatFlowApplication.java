package com.nslhub.chatengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
//@ComponentScan({ "com.thirdwingllc.hwf.controller","com.thirdwingllc.hwf"})
public class ChatFlowApplication extends SpringBootServletInitializer {
	 public static void main(String[] args) {
	        SpringApplication.run(ChatFlowApplication.class, args);
	    }
	 /**
	 	@Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	        return builder.sources(VWFlowApplication.class);
	    }
	    */
	    /**
	   //@PostConstruct
	    public void init() {
		   EmailReciever r = new EmailReciever();
			MyThread myThread = r.new MyThread();
			myThread.start();
	    	
	    }
		*/
}