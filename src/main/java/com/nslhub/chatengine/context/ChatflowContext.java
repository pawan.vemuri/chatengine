package com.nslhub.chatengine.context;

import com.nslhub.chatengine.exception.ChatflowException;


public class ChatflowContext implements IChatflowContext{
	public ChatflowContext(String user) throws ChatflowException{
		this.user = user;
		
		
	}
	private String user = null;
	private boolean admin;
	private boolean manager;
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	@Override
	public String getUserDisplayName() {
		return "thirdwing llc";
	}

	@Override
	public boolean isAdmin() {
		return this.admin;
	}

	@Override
	public boolean isManager() {
		return this.manager;
	}
}
