package com.nslhub.chatengine.context;

import java.io.Serializable;

/**
 * context that is used when executing commands in workflow engine. Caller should authenticate the user and then create a context for the user.
 * @author pawan
 *
 */
public interface IChatflowContext extends Serializable {
 
/**
 * get the user id
 * @return
 */
  public String getUser();

  /**
   * get the user display name
   * @return
   */
  public String getUserDisplayName();
  
  /**
   * return true if this user is an admin.
   * @return
   */
  public boolean isAdmin();

  /**
   * return true if this user is a manager of other users
   * @return
   */
  public boolean isManager();
  
}


