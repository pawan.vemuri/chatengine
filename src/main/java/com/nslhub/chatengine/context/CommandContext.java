
package com.nslhub.chatengine.context;
/**
 * context used to execute commands
 * @author pkvemuri
 *
 */
public class CommandContext {

private IChatflowContext iHwfContext;

/**
 * 
 * @param iHwfContext
 */
public void setHwfContext(IChatflowContext iHwfContext) {
	this.iHwfContext = iHwfContext;
}

/**
 * 
 * @return IWorkflowContext
 */
public IChatflowContext getHwfContext(){
	  return this.iHwfContext;
  }
}
