package com.nslhub.chatengine.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "chattaskhistory")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)

public class ChatTaskHistory {
	public ChatTaskHistory() {
		
	}
	 @Id
	  public String taskId;
	  @Transient
	  public String orgId;
	  @JsonIgnore
	  @Transient
	  public RoutingDef routingDef;
	  public String state;
	  public String participantType;
	  //this id will be unique for a task flow as the task navigates across assignments and other state changes.
	  public String uuid; 
	  public int participantIdx;
	  public String sequentialIdx;
	  public int chatdefid;
	  @Transient
	  public Def wfDef;
	  public String title;
  
}
