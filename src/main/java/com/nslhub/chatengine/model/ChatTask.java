package com.nslhub.chatengine.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.utils.TaskUtils;
@Entity
@Table(name = "chattask")
@EntityListeners(AuditingEntityListener.class)
@Proxy(lazy=false)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class ChatTask {

 /**
  @JsonIgnore
  @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
  @GenericGenerator(name = "native",strategy = "native")
  public int id;
  */
  @Id
  public String taskid;
  public int version;
  @Transient
  public String orgid;
  @JsonIgnore
  @Transient
  public RoutingDef routingDef;
  public String action ;
  public String outcome;
  public Calendar createdDate;
  public String uuid; 
  @JsonIgnore
  public int participantIdx;
  //@JsonIgnore
  //public int blockIdx;
  @JsonIgnore
  public String sequentialIdx;
  @Transient
  public ChatDEf wfDef;
  public String title;
public String state;
public int chatdefid;
public ChatTask() {
	String s = "pawan";
}
  public static ChatTask createChild(ChatTask parent) throws ChatflowException{
	  ChatTask child = new ChatTask();
	  child.routingDef = parent.routingDef;
	  child.createdDate = TaskUtils.getCurrentDate();
	  child.uuid = parent.uuid;
	  child.participantIdx = parent.participantIdx;
	  child.orgid = parent.orgid;
	  child.wfDef = parent.wfDef;
	  child.taskid = TaskUtils.getGuid();
	  return child;
  }
  
  public ChatTask copy() throws ChatflowException{
	  ChatTask child = new ChatTask();
	  child.routingDef = this.routingDef;
	  child.createdDate = TaskUtils.getCurrentDate();
	  child.uuid = this.uuid;
	  child.participantIdx = this.participantIdx;
	  child.orgid = this.orgid;
	  child.wfDef = this.wfDef;
	  return child;
	
  }

}
