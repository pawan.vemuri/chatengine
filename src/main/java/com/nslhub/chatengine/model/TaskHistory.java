package com.nslhub.chatengine.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "hwftaskhistory")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)

public class TaskHistory {
	public TaskHistory() {
		String s = "pawanb";
	}
	 @Id
	 @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
	 @GenericGenerator(name = "native",strategy = "native")
	  public int id;
	  public String taskId;
	  public int version;
	  @Transient
	  public String orgId;
	  @JsonIgnore
	  @Transient
	  public RoutingDef routingDef;
	  public String action ;
	  public String currentAssignee;
	  public String currentAssigneeType;
	  @JsonIgnore
	  public int payloadId;
	  @JsonIgnore
	  public int routingId;
	  public String state;
	  public String participantType;
	  public String outcome;
	  public Calendar createdDate;
	  public Calendar endDate;
	  public Calendar expirationDate;
	  public Calendar updatedDate;
	  public String groupTaskId;
	  //this id will be unique for a task flow as the task navigates across assignments and other state changes.
	  public String uuid; 
	  public int participantIdx;
	  public int blockIdx;
	  public String sequentialIdx;
	  public int taskNumber;
	  public String votingStr;
	  public String versionComment;
	  public String hwfdefid;
	  @Transient
	  public Routing routing;
	  @Transient
	  public Def wfDef;
	  public String title;
  
}
