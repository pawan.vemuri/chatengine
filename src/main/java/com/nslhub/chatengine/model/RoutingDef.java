package com.nslhub.chatengine.model;

import java.util.ArrayList;
import java.util.List;

import com.nslhub.chatengine.participants.IParticipant;

public class RoutingDef {
 
  public List<IParticipant> participants;
 
  public List<IParticipant> getParticipants(){
          return participants;
          
  }
  public void addParticipant(IParticipant participant){
          if(this.participants == null)
                  this.participants = new ArrayList<IParticipant>();
          this.participants.add(participant);
          
  }


}
