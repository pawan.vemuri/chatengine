package com.nslhub.chatengine.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
/**
 * Flow definition. <br/>Holds all the design metadata of flow
 * @author pawan
 *
 */
@Entity
@Table(name = "Chatflow")
@EntityListeners(AuditingEntityListener.class)

//@JsonDeserialize(using = DefDeserializer.class)
//@JsonSerialize(using = DefSerializer.class)
public class ChatDEf {
	

	/**
	 * a unique id for this definition
	 */
  @Id
  @ApiModelProperty(notes = "Unique identification ID for the definition")
  @JsonProperty("id")
  public int id; //primary key
  
  /**
   * full qualified class names that will be called when the flow state changes.
   */
  public String flow;
  /**
   * Definition title. Assigned users can see the title as the subject
   */
  @ApiModelProperty(notes = "Definition title. Assigned users will see the title")
  public String title;
  public String extractionModel;
  public String changeDrivers;
  public String utterances;
 
 
 
}
