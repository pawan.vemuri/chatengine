package com.nslhub.chatengine.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nslhub.chatengine.utils.StringListConverter;

import io.swagger.annotations.ApiModelProperty;
/**
 * Flow definition. <br/>Holds all the design metadata of flow such as title, routing , payload ....
 * @author pawan
 *
 */
@Entity
@Table(name = "hwfdef")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt","rulesDef"},
        allowGetters = true)
//@JsonDeserialize(using = DefDeserializer.class)
//@JsonSerialize(using = DefSerializer.class)
public class Def {
	

	/**
	 * a unique id for this definition
	 */
  @NotBlank
  @Id
  @ApiModelProperty(notes = "Unique identification ID for the definition")
  @JsonProperty("defId")
  public String defId; //primary key
  
  
  @Column
  @Convert(converter = StringListConverter.class)
  public List<String> notifications;
  
  /**
   * Definition title. Assigned users can see the title as the subject
   */
  @ApiModelProperty(notes = "Definition title. Assigned users will see the title")
  public String title;
  /**
   * Definition description
   */
  @ApiModelProperty(notes = "Definition description")
  public String description;
  
  /**
   * Definition payload. For example, for a expense approval flow,  this can be the json structure representing the expense item along with the expense line items.
   */
  @ApiModelProperty(notes = "Definition payload. For example, for a expense approval flow,  this can be the json structure representing the expense item along with the expense line items.")
  @JsonProperty("payloadDef")
  
  public String payloadDef;
  /**
   * payload type. valid values are
   * {@link PAYLOAD_TYPES}
   */
  @ApiModelProperty(notes = "Definition payload type")
  @JsonProperty("payloadDefType")
  public String payloadDefType;
  /**
   * user id of the user who created this flow definition
   */
  public String creator;
  
 
  
  @Column(nullable = false, updatable = false)
  @Temporal(TemporalType.TIMESTAMP)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  @LastModifiedDate
  private Date updatedAt;
  /**
   * string representation of routing definition
   */
  @JsonProperty("routingDef")
  public String routingDef;
  
  /**
   * expiration duration in seconds
   */
  @JsonProperty("expirationDuration")
  public int expirationDuration;
  
  
 
  
  
 
 
 
}
