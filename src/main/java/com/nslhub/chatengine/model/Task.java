package com.nslhub.chatengine.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.utils.TaskUtils;
@Entity
@Table(name = "hwftask")
@EntityListeners(AuditingEntityListener.class)
@Proxy(lazy=false)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Task {

 /**
  @JsonIgnore
  @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
  @GenericGenerator(name = "native",strategy = "native")
  public int id;
  */
  public String taskId;
  public int version;
  @Transient
  public String orgId;
  @JsonIgnore
  @Transient
  public RoutingDef routingDef;
  public String action ;
  public String currentAssignee;
  public String currentAssigneeType;
  @JsonIgnore
  public int payloadId;
  @JsonIgnore
  public int routingId;
  public String state;
  public String participantType;
  public String outcome;
  public Calendar createdDate;
  public Calendar endDate;
  public Calendar expirationDate;
  public Calendar updatedDate;
  public String creator;
  public String groupTaskId;
  //this id will be unique for a task flow as the task navigates across assignments and other state changes.
  public String uuid; 
  @JsonIgnore
  public int participantIdx;
  @JsonIgnore
  public int blockIdx;
  @JsonIgnore
  public String sequentialIdx;
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO,generator="native")
  @GenericGenerator(name = "native",strategy = "native")
  public int taskNumber;
  public String votingStr;
  public String versionComment;
  public String hwfdefid;
  @Transient
  public Routing routing;
  @Transient
  public Def wfDef;
  public String title;
  public static Task createChild(Task parent) throws ChatflowException{
	  Task child = new Task();
	  child.routingDef = parent.routingDef;
	  child.createdDate = TaskUtils.getCurrentDate();
	  child.groupTaskId = parent.groupTaskId;
	  child.uuid = parent.uuid;
	  child.blockIdx = parent.blockIdx;
	  child.participantIdx = parent.participantIdx;
	  child.hwfdefid = parent.hwfdefid;
	  child.orgId = parent.orgId;
	  child.wfDef = parent.wfDef;
	  child.routingId = parent.routingId;
	  child.payloadId = parent.payloadId;
	  child.taskId = TaskUtils.getGuid();
	  return child;
  }
  
  public Task copy() throws ChatflowException{
	  Task child = new Task();
	  child.routingDef = this.routingDef;
	  child.createdDate = TaskUtils.getCurrentDate();
	  child.uuid = this.uuid;
	  child.blockIdx = this.blockIdx;
	  child.participantIdx = this.participantIdx;
	  child.hwfdefid = this.hwfdefid;
	  child.orgId = this.orgId;
	  child.payloadId = this.payloadId;
	  child.wfDef = this.wfDef;
	  child.routingId = this.routingId;
	  return child;
	
  }

  public void incrementVersion() {
	this.version++;
  }

}
