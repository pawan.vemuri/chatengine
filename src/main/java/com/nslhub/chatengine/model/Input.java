package com.nslhub.chatengine.model;
/**
 * Input to kickstart a flow instance<br/>
 * Following are needed to start a flow instance
 * <ul>
 * <li>input payload</li>
 * <li> one of
 * <ul>
 * <li>flow definition</li>
 * <li>flow definition id</li>
 * </ul>
 * </li>
 * </ul>
 * @author pawan
 *
 */
public class Input {
  /**
   * flow definition id
   */
  public String defId;
  /**
   * flow definition
   */
  public ChatDEf def;

}
