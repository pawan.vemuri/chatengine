package com.nslhub.chatengine.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nslhub.chatengine.dao.repository.ChatHistoryRepository;
import com.nslhub.chatengine.dao.repository.ChatTaskRepository;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.model.ChatTaskHistory;
import com.nslhub.chatengine.model.Task;
import com.nslhub.chatengine.model.TaskHistory;
import com.nslhub.chatengine.utils.BeanUtil;
import com.nslhub.chatengine.utils.TaskUtils;

/**
 * dao for task
 * @author pkvemuri
 *
 */

@Component
public class TaskDAO {

 /**
  * find a task by the task id
  * @param wfid
  * @return
  * @throws ChatflowException
  */
	@Autowired
	   ChatTaskRepository wfTaskRepository;
	@Autowired
	   ChatHistoryRepository wfTaskHistoryRepository;
	
	public static TaskDAO getInstance(){
		return BeanUtil.getBean(TaskDAO.class);
	}
	
  public ChatTask findById(String taskId) throws ChatflowException {
	  try {
		  List<ChatTask> list = wfTaskRepository.findByTaskId(taskId);
		  if(list == null || list.size() == 0)
			  return null;
		  ChatTask task = list.get(0);
		  append(task);
		  return task;
	  } catch(Exception e){
		  throw new ChatflowException(e);
	  }
  }
  
  
  /**
   * find count of tasks assigned to a user and of a user type
   * @param name
   * @param type
   * @return
   * @throws ChatflowException
   */
  public int fetchTasksCountByAssignment(String name, String type) throws ChatflowException {
	  try {
		  return  wfTaskRepository.countByAssignment(name,type);
		  
	  } catch(Exception e){
		  throw new ChatflowException(e);
	  }
  }

  private void append(ChatTask task) throws ChatflowException {
	  
  }
  /**
   * fetch all subtasks of a parallel group
   * @param groupTaskId
   * @return
   * @throws ChatflowException
   */
  public List<ChatTask> fetchSubTasks(String groupTaskId) throws ChatflowException {
	  try {
		 List<ChatTask> list = wfTaskRepository.fetchSubTasks(groupTaskId);
		 for(ChatTask task : list){
			append(task);
		 }
		 return list;
	  } catch(Exception e){
		  ChatflowException wfe = new ChatflowException(e);
		  throw  wfe;
	  } 
  }
/**
   * fetch all open subtasks of a parallel group
   * @param groupTaskId
   * @return
   * @throws ChatflowException
   */
  public List<ChatTask> fetchOpenSubTasks(String groupTaskId) throws ChatflowException {
	  try {
		 List<ChatTask> list = wfTaskRepository.fetchOpenSubTasks(groupTaskId);
		 for(ChatTask task : list){
			append(task);
		 }
		 return list;
	  } catch(Exception e){
		  ChatflowException wfe = new ChatflowException(e);
		  throw  wfe;
	  } 
  }
  
  /**
   * @return
   * @throws ChatflowException
   */
  
  public List<ChatTask> fetchAllOpenTasks() throws ChatflowException {
	  try {
		  List<ChatTask> list =  wfTaskRepository.fetchAllOpenTasks();
		  for(ChatTask task : list){
			append(task);
		  }
		  return list;
	  } catch(Exception e){
		  ChatflowException wfe = new ChatflowException(e);
		  throw wfe;
	  }
  }
  
  public void insert(ChatTask hwfTask) throws ChatflowException {
	  //payload and routing are always saved when we insert a new hwftask.
	  //we can skip these blob operations during updates if these haven't changed.
	  if(hwfTask.taskid == null){
		  hwfTask.taskid = TaskUtils.getGuid();
	  }
	 
		  
	  try {
		  ChatTaskHistory h = new ChatTaskHistory();
		  TaskUtils.copyProperties(hwfTask, h);
		  wfTaskHistoryRepository.save(h);
		  wfTaskRepository.save(hwfTask);
	  } catch(Exception e){
		  e.printStackTrace();
		  ChatflowException wfe = new ChatflowException(e);
		  throw  wfe;
	  } 
  }

 

	
  public void insertPayload(Task task){

		
	}
 

 	@Transactional
	public void completeTask(ChatTask p_task) throws ChatflowException {
		 try {
			 ChatTaskHistory h = new ChatTaskHistory();
			 TaskUtils.copyProperties(p_task, h);
			 wfTaskHistoryRepository.save(h);
			 wfTaskRepository.delete(p_task);
		 } catch(Exception e){
			 ChatflowException wfe = new ChatflowException(e);
			  throw  wfe;
		  }
	}
	public void deleteCompletedSubtasks(String groupTaskId) throws ChatflowException{
		ChatTask task = findById(groupTaskId);
		wfTaskRepository.delete(task);
	}
	public void updateTask(ChatTask hwfTask) throws ChatflowException {
		 //payload and routing are always saved when we insert a new hwftask.
		  //we can skip these blob operations during updates if these haven't changed.
		  try {
			  ChatTaskHistory h = new ChatTaskHistory();
			  TaskUtils.copyProperties(hwfTask, h);
			  wfTaskHistoryRepository.save(h);
			  wfTaskRepository.save(hwfTask);
		  } catch(Exception e){
			  ChatflowException wfe = new ChatflowException(e);
			  throw  wfe;
		  }		
	}

	public List<TaskHistory> fetchLastAssignments(Task task) throws ChatflowException {
		try{
			List<TaskHistory> list =   wfTaskHistoryRepository.fetchLastAssignments(task.uuid);
			return list;
		} catch(Exception e){
			ChatflowException wfe = new ChatflowException(e);
			throw wfe;
		} 
	}
	
	public List<ChatTask> findByUUId(String uuid) throws ChatflowException {
		try{
			List<ChatTask> list =   wfTaskRepository.findByUUId(uuid);
			 for(ChatTask _task : list){
					append(_task);
			}
			return list;
		} catch(Exception e){
			ChatflowException wfe = new ChatflowException(e);
			throw wfe;
		} 
	}

	public void deleteAll(){
		this.wfTaskHistoryRepository.deleteAllInBatch();
		this.wfTaskRepository.deleteAllInBatch();
		
	}
	
	
}
