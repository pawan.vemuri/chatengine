package com.nslhub.chatengine.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nslhub.chatengine.dao.repository.ChatDefRepository;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatDEf;
import com.nslhub.chatengine.utils.BeanUtil;

/**
 * data access for task definition
 * @author pkvemuri
 *
 */

@Component
public class ChatDefDAO {
	@Autowired
	ChatDefRepository chatDefRepository;


public static ChatDefDAO getInstance(){
	return BeanUtil.getBean(ChatDefDAO.class);
}
public void delete(int id) throws ChatflowException{
	  try {
		  chatDefRepository.deleteById(id);
	  } catch(Exception e){
		 e.printStackTrace();
	  }
  }

  public ChatDEf findById(int id) throws ChatflowException {
	  try {
		 ChatDEf hwfDef =  chatDefRepository.findById(id).get();
		 return hwfDef;
	  } catch(Exception e){
		  e.printStackTrace();
	  }
	  return null;
  }


 public ChatDEf save(ChatDEf hwfDef) throws ChatflowException {
	 try {
		  
		chatDefRepository.save(hwfDef);

	  } catch(Exception e){
		  e.printStackTrace();
	  }
  //  hwfDef.persist();
    return hwfDef;
  }


  public ChatDEf update(ChatDEf hwfDef) throws ChatflowException {
	ChatDEf entity = findById(hwfDef.id);
    entity.title = hwfDef.title;
    return entity;
  }

  

 }
