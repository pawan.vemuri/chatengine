package com.nslhub.chatengine.dao.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.model.Task;


public interface  ChatTaskRepository extends JpaRepository<ChatTask,Integer> {
	@Query("select u from ChatTask u where u.taskid=?1")
	List<ChatTask> findByTaskId(String taskId);
	@Query("select u from ChatTask u where u.uuid=?1")
	List<ChatTask> findByUUId(String uuid);
	@Transactional
	@Modifying
	@Query("update Task u set u.payloadId=?1 where u.taskId=?2")
	public void updatePayloadId(int payloadId,String taskid);
	@Transactional
	@Modifying
	@Query("update Task u set u.state=?1 where u.taskId=?2")
	public void updateState(String status,String taskid);
	@Query("select u from Task u where u.groupTaskId=?1 and outcome is null")
	public List<ChatTask> fetchOpenSubTasks(String groupTaskId);
	@Query("select u from Task u where u.groupTaskId=?1 and u.groupTaskId!=u.taskId")
	public List<ChatTask> fetchSubTasks(String groupTaskId);
	@Query("select u from Task u where u.taskNumber in(select max(u.taskNumber)  from u group by u.uuid having u.state != 'COMPLETED')")
	public List<ChatTask> fetchAllOpenTasks();
	@Query("select count(*) from Task u where u.currentAssignee=?1 and u.currentAssigneeType=?2")
	public int countByAssignment(String currentAssignee,String currentAssigneeType);
	
}
