package com.nslhub.chatengine.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nslhub.chatengine.model.ChatTaskHistory;
import com.nslhub.chatengine.model.TaskHistory;


public interface  ChatHistoryRepository extends JpaRepository<ChatTaskHistory,String> {
	@Query("select u from TaskHistory u where u.uuid =?1 and u.state='OUTCOME_UPDATED' order by u.taskNumber desc")
	public List<TaskHistory> fetchLastAssignments(String uuid);
	@Query("select u from TaskHistory u where u.uuid =?1 and u.state='INFO_SOUGHT' order by u.taskNumber desc")
	public List<TaskHistory> fetchInfoRequestedUser(String uuid);
}
