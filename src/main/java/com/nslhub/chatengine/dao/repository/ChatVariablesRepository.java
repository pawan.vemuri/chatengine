package com.nslhub.chatengine.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nslhub.chatengine.model.ChatVariables;




public interface  ChatVariablesRepository extends JpaRepository<ChatVariables,String> {
	

}

