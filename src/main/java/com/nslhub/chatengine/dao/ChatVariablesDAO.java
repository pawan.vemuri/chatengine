package com.nslhub.chatengine.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nslhub.chatengine.dao.repository.ChatVariablesRepository;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatVariables;
import com.nslhub.chatengine.utils.BeanUtil;

/**
 * data access for task variable
 * @author pkvemuri
 *
 */

@Component
public class ChatVariablesDAO {
	@Autowired
	ChatVariablesRepository chatVariablesRepository;


public static ChatVariablesDAO getInstance(){
	return BeanUtil.getBean(ChatVariablesDAO.class);
}
public void delete(String id) throws ChatflowException{
	  try {
		  chatVariablesRepository.deleteById(id);
	  } catch(Exception e){
		 e.printStackTrace();
	  }
  }

  public ChatVariables findById(String id) throws ChatflowException {
	  try {
		  ChatVariables variables =  chatVariablesRepository.findById(id).get();
		 return variables;
	  } catch(Exception e){
		 // e.printStackTrace();
	  }
	  return null;
  }


 public ChatVariables save(ChatVariables variables) throws ChatflowException {
	 try {
		  
		chatVariablesRepository.save(variables);

	  } catch(Exception e){
		  e.printStackTrace();
	  }
  
    return variables;
  }


   }
