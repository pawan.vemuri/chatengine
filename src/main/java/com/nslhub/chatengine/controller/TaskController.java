package com.nslhub.chatengine.controller;

import java.util.List;
import java.util.Optional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nslhub.chatengine.command.impl.UpdateOutcomeCommand;
import com.nslhub.chatengine.context.ChatflowContext;
import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.context.IChatflowContext;
import com.nslhub.chatengine.dao.TaskDAO;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.utils.TaskUtils;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RestController
public class TaskController extends BaseController {

	/**
	 * TODO
	 * update documentation to add examples for all possible update messages. given the complexity of the documentation, does it make sense to break this up into individual operations?
	 * "{ "action": "outcome", "outcome": "accept" }"
	 */
   @ApiOperation(value = "Update a task identified by the task number.",
		    notes = "The update operation is defined in the update message. update message is defined as \n * item 1.\n * <b>bold item 2</b>\n")
  /** @ApiOperation(value = "update a task identified by the task number. "
   		+ "\n\n<br/>Update type is defined in the payload.")*/
   @RequestMapping(value = "/task/{id}/update", method = RequestMethod.POST)
   public ResponseEntity<Object> updateTask(@ApiParam(value = "task number", required = true)@PathVariable("id") String id,@ApiParam(value = "update message", required = true)@RequestBody String updateMessage) throws ChatflowException {
	   String loggedInUser = "chatbot";
	   CommandContext commandContext = getContext(loggedInUser);
	   IChatflowContext context = commandContext.getHwfContext();
	   ChatTask task = TaskDAO.getInstance().findById(id);
	   String ret = null;
	   try {
		if(updateMessage.equals("restart")){
			//go back
		}  else {
			UpdateOutcomeCommand uto = new UpdateOutcomeCommand(task, updateMessage);
			ret = uto.execute(commandContext);
		}  
	} catch (JSONException e) {
		throw new ChatflowException("", e);
	}
    return new ResponseEntity<>(ret, HttpStatus.OK);
   }

   @ApiOperation(value = "get count of tasks assigned to a given user")
   @RequestMapping(value = {"/tasks/count","/tasks/{id}/count"}, method = RequestMethod.GET)
   public ResponseEntity<Object> getTasksCountByAssignment(@ApiParam(value = "userid to fetch the tasks for", required = true)@PathVariable Optional<String> id) throws ChatflowException {
	   /**
	    * TODO
	    * we need a context on behalf of pattern here. when user is not specified, we would get the tasks for the logged in user
	    * when id is specified, we have to make sure that the logged in user is admin user and he is getting the list of tasks for another user.
	    */
	   String loggedInUser = "chatbot";
	   ChatflowContext context = new ChatflowContext(loggedInUser);
	   int count = 0;
	   if(context.isAdmin()){
		   count = TaskDAO.getInstance().fetchAllOpenTasks().size();
	   } else {
		   count = TaskDAO.getInstance().fetchTasksCountByAssignment(loggedInUser, "user");
	   }
	  return new ResponseEntity<>(count, HttpStatus.OK);
   }
   
 
  
  
  
   
   @ApiOperation(value = "get task with a task id")
   @RequestMapping(value = "/task/id/{id}", method = RequestMethod.GET)
   public ResponseEntity<Object> getTask(@ApiParam(value = "task id", required = true)@PathVariable("id") String id) throws ChatflowException {
	   ChatTask task = TaskDAO.getInstance().findById(id);
	  return new ResponseEntity<>(task, HttpStatus.OK);
   }


}