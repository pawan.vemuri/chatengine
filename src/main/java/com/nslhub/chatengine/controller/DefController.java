package com.nslhub.chatengine.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nslhub.chatengine.command.impl.InitiateTaskCommand;
import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.dao.repository.ChatDefRepository;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatDEf;
import com.nslhub.chatengine.model.Def;
import com.nslhub.chatengine.model.Input;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin
@Controller
public class DefController extends BaseController {
   @Autowired
   ChatDefRepository chatDefRepository;
   
   @ApiOperation(value = "Delete a workflow definition")
   @RequestMapping(value = "/hwfdefs/{id}", method = RequestMethod.DELETE, produces = "application/json")
   public ResponseEntity<Object> delete(@ApiParam(value = "workflow definition id", required = true)@PathVariable("id") int id) throws ChatflowException { 
	   chatDefRepository.deleteById(id);
	 return new ResponseEntity<>("Task definition is deleted successsfully", HttpStatus.OK);
   }
   
   @ApiOperation(value = "Save a workflow definition")
   @RequestMapping(value = "/chatflow", method = RequestMethod.POST)
   public ResponseEntity<Def> createOrUpdateHwfDef(@ApiParam(value = "workflow definition in json format", required = true)@RequestBody String chatDef) throws ChatflowException {
	  ChatDEf hwfDef = new ChatDEf();
		JSONObject obj = new JSONObject(chatDef);
		Set<String> keys = obj.keySet();
		for(String s : keys)
			System.out.println(s);
		hwfDef.title = obj.getString("name");
		hwfDef.utterances = obj.get("utrrerances").toString();
		hwfDef.extractionModel = obj.get("extractionModel").toString();
		hwfDef.changeDrivers = obj.get("changeDRivers").toString();
		hwfDef.id = 0;//obj.getInt("id");
		hwfDef.flow = obj.get("block").toString();
		chatDefRepository.saveAndFlush(hwfDef);
	   return new ResponseEntity<>(HttpStatus.CREATED);
   }
   
   
   @ApiOperation(value = "create a running task from a workflow definition")
   @RequestMapping(value = "/chatdefs/{id}/initiate", method = RequestMethod.POST)
   public ResponseEntity<Object> initiateHwfTask(@ApiParam(value = "workflow definition id", required = true)@PathVariable("id") int id,@ApiParam(value = "task payload in json format", required = false)@RequestBody java.util.Optional<String> p_payload) throws ChatflowException {
	   String loggedInUser = "chatbot";
	   CommandContext commandContext = getContext(loggedInUser);
	   Optional<ChatDEf> defs = chatDefRepository.findById(id);
	   Input input = new Input();
		input.def = defs.get();
		InitiateTaskCommand itc = new InitiateTaskCommand(input);
		String task = itc.execute(commandContext);
      return new ResponseEntity<>(task, HttpStatus.OK);
   }
  
   @ApiOperation(value = "get all workflow definitions")
   @RequestMapping(value = "/chatdefs", method = RequestMethod.GET)
   public ResponseEntity<Object> getHwfdefs() throws ChatflowException {
	   List<ChatDEf> defs = null;
	  defs = chatDefRepository.findAll();
	  return new ResponseEntity<>(defs, HttpStatus.OK);
   }
  
  
   
   @ApiOperation(value = "get workflow definition by id")
   @RequestMapping(value = "/chatdefs/{id}", method = RequestMethod.GET)
   public ResponseEntity<Object> getHwfdefById(@ApiParam(value = "workflow definition id", required = true)@PathVariable("id") int id) throws ChatflowException {
	  Optional<ChatDEf> defs = chatDefRepository.findById(id);
      return new ResponseEntity<>(defs.get(), HttpStatus.OK);
   }
   
  
   
}