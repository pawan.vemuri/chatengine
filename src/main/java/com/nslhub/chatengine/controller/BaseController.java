package com.nslhub.chatengine.controller;

import com.nslhub.chatengine.context.ChatflowContext;
import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.exception.ChatflowException;

public class BaseController {
	  /**
	    * once spring security and ldap integration is done, get the authenticated user and set in the context
	  * @return 
	 * @throws ChatflowException 
	    */
	   protected CommandContext getContext(String user) throws ChatflowException{
	 	  CommandContext c = new CommandContext();
	 	  ChatflowContext ctx = new ChatflowContext(user);
	 	  c.setHwfContext(ctx);
	 	  ctx.setUser(user);
	 	  return c;
	   }
	   
	 
}
