package com.nslhub.chatengine.command;

import com.nslhub.chatengine.context.GlobalContext;
import com.nslhub.chatengine.exception.ChatflowException;

public class CommandExecutor {

  public <T> String execute(Command<T> command) throws ChatflowException {
    return command.execute(GlobalContext.getCommandContext());
  }
}