package com.nslhub.chatengine.command.impl;

import com.nslhub.chatengine.command.Command;
import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatDEf;
import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.model.Input;
import com.nslhub.chatengine.model.Task;
import com.nslhub.chatengine.utils.TaskUtils;

/**
 * command to initiate a task instance.
 * @author pawan
 *
 */
public class InitiateTaskCommand implements Command<Task> {
	private Input hwfInput;
	/**
	 * 
	 * @param hwfInput
	 */
	public InitiateTaskCommand(Input hwfInput){
		this.hwfInput = hwfInput;
	}
	@Override
	public String execute(CommandContext commandContext) throws ChatflowException {
		 
		ChatDEf def = hwfInput.def;
		if (def == null) {
				ChatflowException wfe = new ChatflowException("");
	            throw wfe;
        }
		ChatTask task = TaskUtils.createChatTaskFromDefAndInput(def,hwfInput);
		task.createdDate = TaskUtils.getCurrentDate();
        
       	task.action = TaskUtils.TASKACTIONS.INITIATE.toString();
       	String ret = new EvaluateRoutingCommand(task).execute(commandContext);
		return ret;
	        

    		
	        
	}
	

}
