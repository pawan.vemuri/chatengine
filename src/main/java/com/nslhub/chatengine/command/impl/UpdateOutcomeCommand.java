package com.nslhub.chatengine.command.impl;

import java.util.Calendar;

import com.nslhub.chatengine.command.Command;
import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.dao.TaskDAO;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.model.Task;
import com.nslhub.chatengine.utils.TaskUtils;
/**
 * command to update outcome for this task
 * @author pkvemuri
 *
 */
public class UpdateOutcomeCommand  implements Command<Task> {

	private ChatTask task;
	private String response;

	/**
	 * 
	 * @param task
	 * @param response
	 */
	public UpdateOutcomeCommand(ChatTask task,String response){
		this.task = task;
		this.response = response;
	}
	@Override
	public String execute(CommandContext commandContext) throws ChatflowException {
		String retTask = null;
		try {
			retTask = _execute(commandContext);
		} catch(ChatflowException e){
				throw e;
			
		}
		return retTask;
	}
	public String _execute(CommandContext commandContext) throws ChatflowException {
		/**
		 * TODO
		 *
		 * cancel any timers that are present for this task.
		 * schedule new timers for this action
		 */
		//if the task is a notify task, then set the outcome to read.
		task.outcome = response;
		task.action = TaskUtils.TASKACTIONS.UPDATE.toString();
		String taskList = new EvaluateRoutingCommand(task).execute(commandContext);
    		

        return taskList;
	}



}
