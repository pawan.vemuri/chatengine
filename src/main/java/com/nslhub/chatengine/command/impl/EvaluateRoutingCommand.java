package com.nslhub.chatengine.command.impl;

import java.util.List;

import org.json.JSONObject;

import com.nslhub.chatengine.command.Command;
import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.dao.ChatVariablesDAO;
import com.nslhub.chatengine.dao.TaskDAO;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatTask;
import com.nslhub.chatengine.model.ChatVariables;
import com.nslhub.chatengine.participants.IParticipant;
import com.nslhub.chatengine.participants.IRoutingEntity;
import com.nslhub.chatengine.participants.types.AbstractParticipant;
import com.nslhub.chatengine.participants.visitor.ParticipantsVisitor;
import com.nslhub.chatengine.utils.TaskUtils;
/**
 * Determine the task routing based on the action taken on the task. The next will be assigned to the next participant or will be marked as completed if all participants have acted on the task.
 * @author pkvemuri
 *
 */
public class EvaluateRoutingCommand implements Command<List<ChatTask>> {

	private ChatTask task;
	/**
	 * 
	 * @param task
	 */
	public EvaluateRoutingCommand(ChatTask task) {
		this.task = task;
	}

	@Override
	public String execute(CommandContext commandContext) throws ChatflowException {
       
         return evaluate(commandContext);
            

	}
	
   private String evaluate(CommandContext commandContext) throws ChatflowException {
	   ParticipantsVisitor participantsVisitor = new ParticipantsVisitor(task,task.routingDef,commandContext);
	   //get the current particpant
	   IRoutingEntity currentParticipant = participantsVisitor.getCurrentParticipant();
	   if(currentParticipant instanceof AbstractParticipant) {
		   AbstractParticipant abstractParticipant = (AbstractParticipant)currentParticipant;
		   boolean isInitiated = TaskUtils.TASKACTIONS.INITIATE.toString().equals(task.action);
		   if(!isInitiated && abstractParticipant.getVariable()!=null) {
			   ChatVariablesDAO chatVariablesDAO = ChatVariablesDAO.getInstance();
			   ChatVariables chatVariables = chatVariablesDAO.findById(task.taskid);
			   JSONObject vobj = null;
			   if(chatVariables == null) {
				   chatVariables = new ChatVariables();
				   chatVariables.taskid = task.taskid;
				   vobj = new JSONObject();
			   } else {
				   vobj = new JSONObject(chatVariables.variables);
			   }
			   vobj.put(abstractParticipant.getVariable(), task.outcome);
			   chatVariables.variables = vobj.toString();
			   chatVariablesDAO.save(chatVariables);
			   
		   }
	   }
       //get the next block or participant
       IRoutingEntity nextParticipant = participantsVisitor.getNextParticipantOrBlock();
       //keep iterating over visitors till one of the visitors can evaluate the task
       if(nextParticipant == null){
    		  // end of routing, all blocks and participants have been evaluated
    		   completeRouting();
    		  return "";
	   } else {
		   task.participantIdx = nextParticipant.getId();
		   TaskDAO.getInstance().updateTask(task);
    		 return participantsVisitor.visit((IParticipant)nextParticipant);
	   }
       
    }

private void completeRouting() throws ChatflowException {
	
	task.action = TaskUtils.TASKACTIONS.COMPLETE.toString();
	TaskDAO.getInstance().updateTask(task);
	
	
	
}

	
   

}
