package com.nslhub.chatengine.command.impl;

import com.nslhub.chatengine.command.Command;
import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.dao.ChatDefDAO;
import com.nslhub.chatengine.exception.ChatflowException;
import com.nslhub.chatengine.model.ChatDEf;
import com.nslhub.chatengine.model.Def;

/**
 * command to persist a task definition
 * @author pkvemuri
 *
 */
public class CreateDefCommand implements Command<Def> {
	private ChatDEf hwfDef;
	/**
	 * 
	 * @param hwfDef
	 */
	public CreateDefCommand(ChatDEf hwfDef){
		this.hwfDef = hwfDef;
	}
	@Override
	public String execute(CommandContext commandContext) throws ChatflowException {
		ChatDefDAO hwfDefDAO =  ChatDefDAO.getInstance();
		hwfDefDAO.save(hwfDef);
		return "";
	}

}
