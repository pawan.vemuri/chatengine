package com.nslhub.chatengine.command;

import com.nslhub.chatengine.context.CommandContext;
import com.nslhub.chatengine.exception.ChatflowException;
/**
 * command interface. 
 * Individual actions like initiate, update will implement this interface.
 * @author pkvemuri
 *
 * @param <T>
 */
public interface Command <T> {

	  String execute(CommandContext commandContext) throws ChatflowException;
	  
	}
