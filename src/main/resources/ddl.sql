CREATE TABLE `chatflow` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `flow` varchar(12550) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `chatvariables` (
  `taskid` varchar(255),
  `variables` varchar(12550) DEFAULT NULL,
  
  PRIMARY KEY (`taskid`)
);

CREATE TABLE `chattask` (
  `taskid` varchar(45) NOT NULL,
  `payloadId` int DEFAULT NULL,
  `routingId` int DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `current_assignee` varchar(45) DEFAULT NULL,
  `current_assignee_type` varchar(10) DEFAULT NULL,
  `version` int DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `participant_type` varchar(45) DEFAULT NULL,
  `outcome` varchar(45) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `group_task_id` varchar(45) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  `participant_idx` int DEFAULT NULL,
  `sequential_idx` varchar(10) DEFAULT NULL,
  `blockIdx` int DEFAULT NULL,
  `chatdefid` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`taskid`)
);

CREATE TABLE `chattaskhistory` (
  `task_id` varchar(45) NOT NULL,
  `payloadId` int DEFAULT NULL,
  `routingId` int DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `current_assignee` varchar(45) DEFAULT NULL,
  `current_assignee_type` varchar(10) DEFAULT NULL,
  `version` int DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `participant_type` varchar(45) DEFAULT NULL,
  `outcome` varchar(45) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `group_task_id` varchar(45) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  `participant_idx` int DEFAULT NULL,
  `sequential_idx` varchar(10) DEFAULT NULL,
  `blockIdx` int DEFAULT NULL,
  `chatdefid` int DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
);

insert into chatflow values(0,'{ "participants": [ { "name": "Choice1", "pType": "SINGLE", "choice": 1, "id":1,"url": "url1", "question": "question1" } ] }','test1');
insert into chatflow values (0,'

{
  "participants": [
    {
       "id":1,
       "name": "Choice",
      "pType": "SINGLE",
      "url": "url1",
      "question": "enter your phone number",
      "variable":"phNo"
    },
    {
      "name": "Choice0",
      "id":2,
      "pType": "PARALLEL",
      "participants": [
        {
          "id":3,
          "name": "Choice1",
          "pType": "SINGLE",
          "choice": 1,
          "desc" " "savings account balance",
          "url": "url3",
          "question": "Your saving account balance is {http://localhost:8080/balance/$phNo}"
        },
        {
          "id":4,
          "name": "Choice2",
          "pType": "SINGLE",
          "choice": 2,
          "desc" " "credit card balance",
          "url": "url4",
          "question": "Your saving account balance is {http://localhost:8080/balance/$phNo}."
        },
        
      ]
    }
  ]
}','test2');

